import React, {Component} from 'react';
import Header from "./components/sections/Header";
import {Route, Switch} from "react-router-dom";
import './css/bootstrap.min.css';
import './css/bootstrap-rtl.min.css';
import './css/main.css'
import ScrollToTop from "react-scroll-up";
import topSvg from './images/up.png'

// Components
import Home from "./components/pages/Home";
import Contact from "./components/pages/Contact";
import Product from "./components/pages/Product";
import NoMatch from "./components/pages/NoMatch";
import Slider from "./components/sections/Slider";
import Footer from "./components/sections/Footer";
import Category from "./components/pages/Category";

class App extends Component {
    changeRoute() {
        document.title = 'روزنوشته‌های حسام‌ کاوه';  // set title to default
    }

    render() {
        return (
            <div>
                <ScrollToTop showUnder={290}>
                    <span id="goUp">
                        <img src={topSvg} alt="go up"/>
                    </span>
                </ScrollToTop>
                <Header/>
                <div className="row rtl">


                    <div className="col routeContainer">
                        <Switch>
                            <Route path="/" exact={true} component={Home} onClick={this.changeRoute()}/>
                            <Route path="/contact" component={Contact} onClick={this.changeRoute()}/>
                            <Route path="/category/:catName" component={Category} onClick={this.changeRoute()}/>
                            <Route path="/:title" component={Product} onClick={this.changeRoute()}/>
                            <Route component={NoMatch} onClick={this.changeRoute()}/>
                        </Switch>
                    </div>
                    <div className="col slider"><Slider/></div>
                </div>
                <Footer/>

            </div>
        );
    }
}

export default App;

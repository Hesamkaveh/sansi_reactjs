import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import NavItem from "../NavItem";
import logo from "../../images/logo.jpg"
class Header extends Component {
    render() {
        return (
            <div style={{textAlign: 'center'}}>
                <Link to=''><img className="logo" src={logo} alt=""/></Link>
                <div>روز نوشته های یک توسعه دهنده</div>
                <div className={"rtl navbarContainer text-center"}>
                    <NavItem activeOnlyWhenExact={true} activeClassName="selected" to="/">بلاگ</NavItem>
                    <NavItem to="/contact">درباره حسام</NavItem>
                </div>
            </div>
        );
    }
}

export default Header;

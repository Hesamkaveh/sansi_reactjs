import React, {Component} from 'react';
import {Link} from "react-router-dom";
import dateSvg from "../images/calendar.svg";
import commentSvg from "../images/comment.svg";
import ReactHtmlParser from 'react-html-parser';

class Product extends Component {
    render() {
        const {product} = this.props;
        return (
            <div className="row everyPostContainer" style={{marginBottom: 20}}>
                <div className="pc">
                    <h1>
                        <Link className="postTitle" to={`${product.title}`}>
                            {product.title}
                        </Link>
                    </h1>
                    <div className="postDescriptin">
                        <span>
                            <img src={dateSvg} alt="logo"/>
                            {'\u00A0'}
                            {product.date_day}{'\u00A0'}
                            {product.date_month}{'\u00A0'}
                            {product.date_year}{'\u00A0'}

                        </span>
                        {'\u00A0'}{'\u00A0'}
                        <span>
                            <img src={commentSvg} alt="logo"/> {'\u00A0'}
                            {product.comments} کامنت
                        </span>


                    </div>
                    <div className="postContent">
                        {ReactHtmlParser(product.content.substr(0, 700))}

                        </div>
                    <Link className="continueReadingBtn" to={`${product.title}`}>ادامه مطلب←</Link>
                </div>
            </div>
        );
    }
}

export default Product;

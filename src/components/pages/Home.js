import React, {Component} from 'react';
import axios from 'axios';
import Product from './../Product';
import InfiniteScroll from 'react-infinite-scroller';
import loadingIcon from '../../images/Loading_icon.gif'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            articles: [],
            nextPage: 1,
            hasMore: true,
            display_loader: 'block',
        }
    }

    componentWillMount() {
        // document.title = `${document.title}`;
    }

    handleLoadMore() {


        axios.get(`https://be.hesamkaveh.com/getBlogPosts?querySize=3&nextId=${this.state.nextPage}`)
        // axios.get(`http://roocket.org/api/products?page=${this.state.nextPage}`)
            .then(response => {
                const {hasMore, data} = response.data;
                this.setState(prevState => ({
                    articles: [...prevState.articles, ...data],
                    hasMore: hasMore,
                    nextPage: this.state.nextPage + 3,
                    display_loader: 'none'
                }))
            })
            .catch(error => {
                console.log(error)
            })
    }


    render() {
        return (
            <div className="rtl">
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.handleLoadMore.bind(this)}
                    // useWindow={0}
                    hasMore={this.state.hasMore}
                    loader={<img src={loadingIcon} className="loader"
                                 style={{
                                     position: 'absolute',
                                     left: '50%',
                                     width: 50,
                                     top: '100px',
                                     display: this.state.display_loader
                                 }} alt=""/>}
                >
                    {this.state.articles.map((product, index) => <Product product={product} key={index}/>)}
                </InfiniteScroll>


            </div>
        );
    }
}

export default Home;

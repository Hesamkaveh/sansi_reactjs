import React, {Component} from 'react';
import axios from "axios/index";
import Product from "../Product";

class Category extends Component {

    constructor(props) {
        super(props);
        this.state = {
            posts: []
        };
    }

    queryData(catName) {
        axios.get(`https://be.hesamkaveh.com/getPostsOfACategory?catName=${catName}`)
            .then(response => {
                this.setState({
                    posts: response.data.data
                });
            })
            .catch(error => {
                console.log(error);
            })

    }

    componentDidMount() {
        document.title = `${this.props.match.params.catName} | ${document.title}`;
        this.queryData(this.props.match.params.catName)
    }

    componentWillReceiveProps(nextProps) {
        this.queryData(nextProps.match.params.catName)
    }

    render() {
        return (
            <div className="rtl">
                {this.state.posts.map((product, index) => (<Product product={product} key={index}/>))}
            </div>
        );
    }
}


export default Category;

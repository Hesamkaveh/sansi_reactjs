import React, {Component} from 'react';
import axios from 'axios';
import dateSvg from "../../images/calendar.svg";
import commentSvg from "../../images/comment.svg";
import tag from "../../images/tag-black.svg";
import ReactHtmlParser from 'react-html-parser';
import InlineSVG from 'react-inlinesvg';
import MetaTags from 'react-meta-tags';


class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            article: {},
            tagsArray: [],
        };
    }

    queryData(title) {
        axios.get(`https://be.hesamkaveh.com/getBlogPost?title=${title}`)
            .then(response => {
                this.setState({
                    article: response.data
                })
            })
            .catch(error => {
                console.log(error);
            })

    }

    componentWillMount() {
        const {params} = this.props.match;

        document.title = `${params.title} | ${document.title}`;
        this.queryData(this.props.match.params.title)
    }

    componentWillReceiveProps(nextProps) {
        this.queryData(nextProps.match.params.title)

    }

    render() {
        const {article} = this.state;
        for (var key in article.tags) {
            this.state.tagsArray.push(article.tags[key]);
        }
        return (
            <div className="rtl everyPostContainer">

                <MetaTags>
                    {this.state.tagsArray.map((s, index) => (<meta property="article:tag" content={s}/>))}
                    {<meta name="description" content={article.description}/>}
                </MetaTags>


                <h1 className='postTitle2'>
                    {article.title}
                </h1>
                <div className="postDescriptin">
                        <span>
                            <img src={dateSvg} alt="logo"/>
                            {'\u00A0'}
                            {article.date_day}{'\u00A0'}
                            {article.date_month}{'\u00A0'}
                            {article.date_year}{'\u00A0'}
                        </span>
                    {'\u00A0'}{'\u00A0'}
                    <span>
                            <img src={commentSvg} alt="logo"/> {'\u00A0'}
                        {article.comments} کامنت
                        </span>


                </div>


                <div className="postContent">
                    {ReactHtmlParser(article.content)}
                </div>
                <hr/>
                <div className="tags">
                    <InlineSVG src={tag} className=''/>
                    {this.state.tagsArray.map((s, index) => (<a className='followMeContainer ' href="/">{s}</a>))}
                </div>
            </div>
        );
    }
}

export default Product;

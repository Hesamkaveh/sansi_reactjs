import React, {Component} from 'react';
import avatar from '../../images/avatar.png'

class Contact extends Component {
    componentWillMount() {
        document.title = `درباره من | ${document.title}`;
    }

    render() {
        return (
            <div className="rtl everyPostContainer">

                <h2 style={{textAlign: 'center'}}>درباره من</h2>
                <p>
                    <img src={avatar} alt="" style={{
                        margin: '0 auto',
                        display: 'block',
                        width: 150
                    }}/>
                </p>
                <p>
                    حسام کاوه هستم. تقریبا از 5 سالگی با کامپیوتر دست و پنجه نرم میکنم. اون اوایل تصورم از آینده این بود
                    که
                    میشینم پشت میز، فیلم میبینم، چیپس میخورم و زندگیمو میکنم. اما کم کم کار به جایی رسید که بعد از
                    فیلم
                    دیدن نکات جالب و آموزندشو یادداشت میکنم که تو زندگیم بکار ببرم تا حس نکنم با فیلم دیدن وقتم هدر
                    رفته!
                </p>
                <p>
                    ارادت خاصی هم به ورزش دارم، هرچی میخواد باشه باشه اما دوچرخه برام یچیز دیگست. یادمه دوران طفولیت
                    سوار بر
                    دوچرخه قایم باشک بازی میکردم!!! یک مدت برای مسابقات رکاب میزدم اما واقعیت اینه که ورزش شغل من نیست و
                    کمی
                    خوش گذرونی و سلامتی برام کافیه. البته راستشو بخواید دوچرخه سواری تو زندگی و کارمم بهم کمک کرده چون
                    بهم
                    یاد داده برا چیزی که میخوام باید بجنگم و عرق بریزم.
                </p>
                <p>
                    کارشناسی نرم افزار دانشگاه اراک میخونم ولی تابحال به درس خوندن افتخار نکردم. دانشگاه تا یه حدیش خوبه
                    و
                    آدمو رشد میده ولی بیشتر از اون بیخودیه! اگه برنامه نویسی رو هم نوعی ورزش حساب کنیم، بیشتر وقتمو
                    مشغول
                    ورزشم! به لینوکس و پایتون و جاوااسکریپت علاقه خاصی دارم.
                </p>
                <p>
                    مطالعه و کتاب‌خوانی از لذت‌بخش ترین کارهاییه که در طول روز انجام میدم و عقیده دارم ذهن رو باز میکنه
                    و کلی چیز به آدم یاد میده. برتری خودآموزی نسبت به دانشگاه اینه که خودتون انتخاب می‌کنید چی یاد
                    بگیرید.
                </p>
                <p>
                    تقریبا در اکثر شبکه های اجتماعی می‌تونید منو پیدا کنید ولی بهترین و سریع‌ترین راه ارتباطی با من
                    hk.hesam.hk[at]gmail.com است.
                </p>
            </div>
        );
    }
}

export default Contact;
